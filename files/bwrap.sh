#!/bin/bash

set -eu

if flatpak-spawn --host bwrap --version &>/dev/null; then
    BWRAP=bwrap
else
    BWRAP=/usr/libexec/flatpak-bwrap
fi

args=()
fds=(1 2 3)
while [ "$#" -gt 0 ]; do
    case "$1" in
        --args|--userns|--userns2|--pidns|--sync-fd|--seccomp|--block-fd|--userns-block-fd|--info-fd)
            args+=("$1" "$2")
            fds+=("$2")
            shift 2
            ;;
        --file|--bind-data|--ro-bind-data)
            args+=("$1" "$2" "$3")
            fds+=("$2")
            shift 3
            ;;
        --uid|--gid|--hostname|--chdir|--unsetenv|--lock-file|--remount-ro|--proc|--dev|--tmpfs|--mqueue|--dir|--exec-label|--file-label|--cap-add|--cap-add)
            args+=("$1" "$2")
            shift 2
            ;;
        --symlink|--setenv)
            args+=("$1" "$2" "$3")
            shift 3
            ;;
        --)
            args+=("$@")
            shift "$#"
            ;;
        *)
            args+=("$1")
            shift
            ;;
    esac
done

spawn_args=()
for fd in "${fds[@]}"; do
    spawn_args+=("--forward-fd=${fd}")
done

exec flatpak-spawn --host "${spawn_args[@]}" "${BWRAP}" "${args[@]}"
