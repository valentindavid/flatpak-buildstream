#!/bin/bash

exec flatpak-spawn \
     --host \
     --forward-fd=1 \
     --forward-fd=2 \
     --forward-fd=3 \
     ${_FUSE_COMMFD:+--env=_FUSE_COMMFD=${_FUSE_COMMFD} \
                     --forward-fd=${_FUSE_COMMFD}} \
     fusermount "$@"
