#!/usr/bin/env python3

import sys
import os
import subprocess

pipe = os.pipe()

def set_pgrp():
    os.close(pipe[0])
    os.setpgrp()
    os.write(pipe[1], b'\0')
    os.close(pipe[1])

p = subprocess.Popen(['bst'] + sys.argv[1:], preexec_fn=set_pgrp)

os.close(pipe[1])
os.read(pipe[0], 1)
os.close(pipe[0])
pgrp = os.getpgid(p.pid)
os.tcsetpgrp(0, pgrp)
r = p.communicate()
sys.exit(p.returncode)
